using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace PolynomialObject.Exceptions
{
    [Serializable]
    public class PolynomialArgumentNullException : Exception
    {
        public PolynomialArgumentNullException() { }

        public PolynomialArgumentNullException(string message) { }
    }
}
